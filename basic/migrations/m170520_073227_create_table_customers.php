<?php

use yii\db\Migration;

class m170520_073227_create_table_customers extends Migration
{
    public function up()
    {
        $this->createTable('customers', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'email' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('customers');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
