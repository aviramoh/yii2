<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movie`.
 */
class m170609_061223_create_movie_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('movie', [
            'id' => $this->primaryKey(),
			'name' => $this->String(),
			'genre' => $this->String(),
			'min_age' => $this->integer(),
			'score' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movie');
    }
}
