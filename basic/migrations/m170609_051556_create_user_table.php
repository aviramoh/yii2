<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170609_051556_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'username' => $this->String(),
			'password' => $this->String(),
			'auth_key' => $this->String(),
			
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
