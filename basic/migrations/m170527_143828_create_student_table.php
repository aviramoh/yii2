<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m170527_143828_create_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('student', [
            'key' => $this->primaryKey(),
			'name' => $this->string(),
			'id' => $this->integer(),
			'age' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('student');
    }
}
