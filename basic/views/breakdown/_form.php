<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Level;
use app\models\Status;


/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php 	if ($model->isNewRecord)
				echo $form->field($model, 'levelId')->dropDownList([Level::getLevels()], ['options' => [3 => ['disabled' => true]]]) ?>

     <?php	if (!$model->isNewRecord && \Yii::$app->user->can('updateBrkDwn')) {//appears only on update	
				echo $form->field($model, 'statusId')->dropDownList(Status::getStatuses());
				echo $form->field($model, 'levelId')->dropDownList(Level::getLevels());	 }?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
