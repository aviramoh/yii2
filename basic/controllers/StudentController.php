<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;
use yii\db\ActiveRecord;


class StudentController extends Controller
{
	/*public function actionView($id)
    {
		$name = Student::getName($id); //we use :: beacuse its a static function
		return $this->render('view', ['name' => $name]);
    }*/

	public function actionView($key)
	{
		$student = Student::getObject($key);
		return $this->render('view', ['student' => $student]);
		/*$name = $student->name;
		$id = $student->id;
		$age = $student->age;*/
		/*return $this->render('view', ['name' => $name, 'id' => $id, 'age' => $age]);*/
	}

	public function actionShow()
	{
		$student = Student::getAll(); //get array of objects from model
		return $this->render('show', ['student' => $student]);
	}	
}
