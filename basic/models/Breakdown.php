<?php

namespace app\models;

use Yii;
use app\models\Level;
use app\models\Status;

/**
 * This is the model class for table "breakdown".
 *
 * @property integer $id
 * @property string $title
 * @property integer $levelId
 * @property integer $statusId
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['levelId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert); //must be here
		
        if ($this->isNewRecord) 
		    $this->statusId = 1;

	    return $return;
    }
	
		public function getLevelItem()
	{
		return $this->hasOne(Level::className(), ['id' => 'levelId']);
	}
	
	public function getStatusItem()
	{
		return $this->hasOne(Status::className(), ['id' => 'statusId']);
	}
	
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'levelId' => 'Level ID',
            'statusId' => 'Status ID',
        ];
    }

}
