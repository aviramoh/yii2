<?php
//OwnUserRule
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnUserRule extends Rule
{
	public $name = 'ownUserRule'; //must be here

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) { //is loged in
			return isset($params['user']) ? $params['user']->id == $user : false;
		}
		return false;
	}
}