migration codes:
1. create new table:  php yii migrate/create create_user_table
2. add columns to existing table: php yii migrate/create alter_user_tables

<?php
								public function up()
						{
							$this->addColumn('user','firstname','string');
							$this->addColumn('user','lastname','string');
						}

						public function down()
						{
							$this->dropColumn('user','firstname');
							$this->dropColumn('user','lastname');
						}
?>
3. droptable:  php yii migrate drop_user_table
**don't forget! after writing in notepade: php yii migrate

----------------------------------------------------------------------------------

RBAC:
1. creating auth tables (suppose to existing all ready):

						//RBAC config should be located in web.php and console.php
						'authManager' => [
								'class' => 'yii\rbac\DbManager',
						],	
									
						//RBAC migration 
						php yii migrate --migrationPath=@yii/rbac/migrations
						
2. creating a rule:
	a. create a new directory: rbac
	b. create a new rule file: OwnUserRule.php
<?php							
						namespace app\rbac;
						use yii\rbac\Rule;
						use Yii; 

						class OwnUserRule extends Rule
						{
							public $name = 'ownUserRule';	
							public function execute($user, $item, $params)
							{
								if (!Yii::$app->user->isGuest) {
									return isset($params['user']) ? $params['user']->id == $user : false;
								}
								return false;
							}
						} ?>
							
	c. go to commands -> create new file: AddruleController.php
						<?php
						namespace app\commands;

						use Yii;
						use yii\console\Controller;

						class AddruleController extends Controller
						{
							public function actionOwnuser()
							{	
								$auth = Yii::$app->authManager;	
								$rule = new \app\rbac\OwnUserRule;
								$auth->add($rule);
							}
						}?>
						
	d. run command in putty: php yii addrule/ownuser
	e. attach rule in auth_item table (Cpanel)
	f. contoller: to the matching action:
<?php	              
						use yii\web\UnauthorizedHttpException;
						
						if (!\Yii::$app->user->can('updateUser', ['user' =>$model]) )
						throw new UnauthorizedHttpException ('Hey, You are not allowed to update a user that is not your own');
?>

----------------------------------------------------------------------------------

Dropdown ->form
<?php
//models/Status.php 
use yii\helpers\ArrayHelper;

	public static function getStatuses()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'id', 'name');
		return $allStatusesArray;						
	}

//views/lead/_form.php האוביקט שאליו מקושר הסטטוס

use app\models\Status;

	<?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?>  


//models/Lead.php במידה וצריך תאריך, שעה ושם

use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

	 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

//models/Lead.php 

    /**
     * Defenition of relation to user table
     */ 	
	
	public function getUserOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
    /**
     * Defenition of relation to status table
     */  
 	
 
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
//view/lead/view.php תצוגה של רשומה בודדת

			[ // the status name 
				'label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->name,	
			],			
			[ // the owner name of the lead
				'label' => $model->attributeLabels()['owner'],
				'format' => 'html',
				'value' => Html::a($model->userOwner->fullname, 
					['user/view', 'id' => $model->userOwner->id]),	
			],
			[ // Lead created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],
			[ // Lead created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],				
			[ // Lead updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
			[ // Lead updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],			
?>
----------------------------------------------------------------------------------

הוספה ושמירה שדה רול

//models/User.php

	public $role; //שורה ראשונה אחרי שם המחלקה

	['role', 'safe'], //שורה אחרונה בפונקציה RULES

	public static function getRoles()
	{

		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		
		return $roles; 		
	}	


	public function afterSave($insert,$changedAttributes) //שומר את התפקיד בבסיס נתונים
    {
        $return = parent::afterSave($insert, $changedAttributes);

		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			$auth->assign($role, $this->id);
		} else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }	

	//אם לא מעדכנים את הרול בעט יצירת המשמש
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

		$auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			if($role != null){
				$auth->assign($role, $this->id);
			}
		} 
		else {
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment',
				['user_id' => $this->id])->execute();
			$auth->assign($role, $this->id);
		}

        return $return;
    }
	
//view/User/_form.php
//הצגת שדה רול רק בעט עדכון האוביקט
	<?php if (!$model->isNewRecord) { ?>
	<?= $form->field($model, 'role')->dropDownList(User::getRoles()) ?>
	<?php } ?>

----------------------------------------------------------------------------------

Dropdown in search box

<?php 
// models/Status.php
	public static function getStatusesWithAllStatuses()
	{
		$allStatuses = self::getStatuses();
		$allStatuses[null] = 'All Statuses';
		$allStatuses = array_reverse ( $allStatuses, true );
		return $allStatuses;	
	}

// controllers/LeadController.php

use app\models\Status;
//in action Index after dataProvider
		'statuses' => Status::getStatusesWithAllStatuses(),
		'status' => $searchModel->status,
	
// 	views/lead/index.php in GridView::width
            [
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('LeadSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],

// models/Lead ?????
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status, //add this line
			
//+ add status to validation rules 			
			
----------------------------------------------------------------------------------

NAV BAR
//views/layout/main

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => Yii::$app->user->isGuest ?  [
			['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
			['label' => 'Login', 'url' => ['/site/login']],
			['label' => 'Users', 'url' => ['/user/index']],
		]:[
		    ['label' => 'Activities', 'url' => ['/activity/index']],
            ['label' => 'Users', 'url' => ['/user/index']],
			    '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'		
		] 
    ]);
	
	
//config/web.php	הגדרת דף דיפולטיבי אחרי בוטסטראפ

'homeUrl' => ['lead/index'],

----------------------------------------------------------------------------------

Insert value automaticly with no defining

//models/activity
public function beforeSave($insert)
    {
	
		if(parent::beforeSave($insert))
		{
			if ($this->isNewRecord)
			{
				$model = new Activity;
				$this->statusId = 2;
		}
	}	
		return parent::beforeSave($insert);
	}
	
----------------------------------------------------------------------------------

אי שמירה של נתונים במסד נתונים

//models/activity	

    public function rules()
    {
    $rules = []; 
        $stringItems = [['title'], 'string'];
        $integerItems  = ['statusId'];        
        if (\Yii::$app->user->can('updateActivity')) {
            $integerItems[] = 'catagoryId';
        }
        $integerRule = [];
        $integerRule[] = $integerItems;
        $integerRule[] = 'integer';
        $rules[] = $stringItems;
        $rules[] = $integerRule;       
        return $rules;  
    }
	
----------------------------------------------------------------------------------
התניות

1. בעל הרשאה מסויימת
<?php if (\Yii::$app->user->can('changeCategory')) { ?>

<?php } ?>

2. האם אורח
if(Yii::$app->user->isGuest)

3. האם יצירה ולא עדכון
<?php if ($model->isNewRecord) { ?>

<?php } ?>		

----------------------------------------------------------------------------------

upload to Bitbucket in putty
1. git add -A
2. git commit -m"comment"
3. git push

----------------------------------------------------------------------------------
